---
home: true
heroImage: images/logo.png
heroText: Cooking Technical Assessment
actionText: Get Started →
actionLink: /home.md
footer: COPYRIGHT © Christopher Zeuch 2021 - Present
---
