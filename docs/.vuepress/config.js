module.exports = {
  title: 'Cooking Technical Assessment',
  description: ' ',
  base: '/cooking-technical-assessment/',
  dest: './public',
  head: [
    ['link', { rel: 'icon', href: 'images/logo.png' }]
  ],
  plugins: [],
  theme: 'yuu',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/home.md' },
      { text: 'Test Plan', link: '/test-plan.md' },
      { text: 'Test Cases', link: '/test-cases.md' },
      { text: 'Issue Example', link: '/issue-example.md' },
      { text: 'Testing Best Practises', link: '/testing-best-practises.md' },
      { text: 'Test Case Example Code', link: '/test-case-example-code.md' },
      { text: 'Manual OR Automation', link: '/manual-or-automation.md' },
      { text: 'Test Coverage Report', link: '/test-coverage-report.md' }
    ],
    sidebar: {
      '/': [
        ['home', 'Home'],
        ['test-plan', 'Test Plan'],
        ['test-cases', 'Test Cases'],
        ['issue-example', 'Issue Example'],
        ['testing-best-practises', 'Testing Best Practises'],
        ['test-case-example-code', 'Test Case Example Code'],
        ['manual-or-automation', 'Manual OR Automation'],
        ['test-coverage-report', 'Test Coverage Report'],
        ['stylesheet', 'Stylesheet']
      ]
    },
    sidebarDepth: 3,
    search: true,
    yuu: {
      defaultDarkTheme: true,
      defaultColorTheme: 'blue',
    }
  }
}
