# Welcome

This VuePress repository serves as the report for the Cooking Technical Assessment.

## Expected Output Answers

- A [Test Plan](test-plan.md) has been created
- The [Test Cases](test-cases.md) have been defined
- An [Issue Example](issue-example.md) has been created
- A [Tools](test-plan.md#tools) section has been defined within the Test Plan
- [Testing Best Practises](testing-best-practises.md)
- [Example code for one Test Case](test-case-example-code.md) has been written in Gherkin.
- [Manual OR Automation](manual-or-automation.md) motivation.
- A [Test Coverage Report](test-coverage-report.md) has been created

## My Assumptions before Beginning

- The cooking enthusiast is using a mobile application to access and save recipes
- The mobile application displays recipes which are curated and published by non-users
- The mobile application is an iOS app
- The cooking enthusiast has an account which means a database of some sorts is required to store account information as well as the recipes
- A REST API is used

I have also listed these assumptions in the Test Plan within the [Introduction - Assumptions section](test-plan.md#assumptions).

## How long you took to complete the assignment

The assignment took me roughly `7` hours to complete.

## If you had more time, what would you do differently and additionally?

If I had more time I would have given the backend side of things more attention and defined some more automated test cases specifically for API testing.

I could also expanded on more use cases per feature to improve the coverage.

I would have liked to try out example code for a BrowserStack automated scenario instead of using Gherkin.
