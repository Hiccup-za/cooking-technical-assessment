Feature: Registration

  Scenario: Launch the app with no internet connection
    Given that the device is in an offline state
    When the user launches the app
    Then the app should launch
    And the offline warning should display
    Then the app functionality should become inactive
