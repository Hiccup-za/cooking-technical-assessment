# Stylesheet

## Badges

### Standard Badges

- <Badge text="Tip" type="tip"/>
- <Badge text="Warn" type="warn"/>
- <Badge text="Error" type="error"/>

```html
- <Badge text="Tip" type="tip"/>
- <Badge text="Warn" type="warn"/>
- <Badge text="Error" type="error"/>
```

### Custom Badge Component

- <colorBadge text="Blue" type="blue"/>
- <colorBadge text="Green" type="green"/>
- <colorBadge text="Light Green" type="lightGreen"/>
- <colorBadge text="Light Red" type="lightRed"/>
- <colorBadge text="Orange" type="orange"/>
- <colorBadge text="Pink" type="pink"/>
- <colorBadge text="Purple" type="purple"/>
- <colorBadge text="Red" type="red"/>
- <colorBadge text="Yellow" type="yellow"/>

```html
- <colorBadge text="Blue" type="blue"/>
- <colorBadge text="Green" type="green"/>
- <colorBadge text="Light Green" type="lightGreen"/>
- <colorBadge text="Light Red" type="lightRed"/>
- <colorBadge text="Orange" type="orange"/>
- <colorBadge text="Pink" type="pink"/>
- <colorBadge text="Purple" type="purple"/>
- <colorBadge text="Red" type="red"/>
- <colorBadge text="Yellow" type="yellow"/>

```

This component is defined within the [colorBadge Component][1] and the background colours are defined within [palette.styl][2].

## Containers

::: tip
This is a tip container.
:::

```md
::: tip
This is a tip container.
:::
```

::: warning
This is a warning container.
:::

```md
::: warning
This is a warning container.
:::
```

::: danger
This is a danger container.
:::

```md
::: danger
This is a danger container.
:::
```

Custom styling has been applied to these containers within [index.styl][3].

[1]: https://gitlab.com/Hiccup-za/cooking-technical-assessment/-/blob/master/docs/.vuepress/components/colorBadge.vue
[2]: https://gitlab.com/Hiccup-za/cooking-technical-assessment/-/blob/master/docs/.vuepress/styles/palette.styl
[3]: https://gitlab.com/Hiccup-za/cooking-technical-assessment/-/blob/master/docs/.vuepress/styles/index.styl
