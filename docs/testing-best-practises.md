# Testing Best Practises

I have defined four Best Practises to be applied to this project.

## 1. Define a Test Strategy

Its important to define a Test Strategy that not only works for features in development but one that takes possible patches and hot fixes into account.
We don't want to be in a situation where a fix has to be urgently made, only to have a Test Strategy and process that takes days to go through.

This would allow artifacts like Smoke Tests to be created so that fixes can be tested quickly and go out with confidence.
This strategy will also take things like Stabilization Phases and Hardening Phases into account.

## 2. Use CI/CD

Part of building up confidence is Continuos Integration and Continuos Deployment.

We need to set up pipeline system that execute the automated tests on merge to feature branches and to master.
This will ensure that no incoming code causes a regression.

We also want the pipeline system to automatically deploy the mobile application once the tests have passed.
This ensures that our tools like Browser Stack and Test Flight always contain the latest builds.

## 3. Define Test Management Best Practises

Clearly defined Test Management Best Practises ensure that when someone wants to contribute to Quality & Testing artifacts like Test Plans and test specifications, they know exactly how to go about doing it.
Code reviews will go fast as the focus will be on the value being added.

## 4. Consistent & Clear Communication

Communication is absolutely key when working towards a shared goal.
Everyone involved in the process of building great quality software should feel free to speak to one another.
Sometimes good insights and ideas can come from a team working on a completely different project.
