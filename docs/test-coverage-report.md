# Test Coverage Report

::: tip Legend
The following is a legend describing the point system & the use case color system:

| Coverage                                                                  | Points | Percentage |
| ------------------------------------------------------------------------- | :----: | :--------: |
| <Badge text="Positive" type="tip"/>                                       |  0.5   |    50%     |
| <Badge text="Negative" type="error"/>                                     |  0.5   |    50%     |
| <Badge text="Positive" type="tip"/> <Badge text="Negative" type="error"/> |   1    |    100%    |

| Use Cases                          |     Description     |
| ---------------------------------- | :-----------------: |
| <colorBadge text="x" type="blue"/> | Number of use cases |
:::

## Total Coverage <colorBadge text="61%" type="green"/>

| Feature             |  Manual  |  Automated  |                  Total Coverage                   |      Total Test Specifications      |
| ------------------- | :------: | :---------: | :-----------------------------------------------: | :---------------------------------: |
| Registration        | `0/2=0%` | `1.5/2=75%` |       <colorBadge text="75%" type="green"/>       | <colorBadge text="4" type="blue"/>  |
| Log In & Out        | `0/2=0%` |  `1/2=50%`  |      <colorBadge text="50%" type="orange"/>       | <colorBadge text="2" type="blue"/>  |
| Recipes             | `0/3=0%` | `1.5/3=50%` |      <colorBadge text="50%" type="orange"/>       | <colorBadge text="3" type="blue"/>  |
| Notes & Adjustments | `0/2=0%` | `2/2=100%`  |      <colorBadge text="100%" type="green"/>       | <colorBadge text="4" type="blue"/>  |
| **Total**           |  `0/9`   |   `5.5/9`   | `5.5/9=61%` <colorBadge text="61%" type="green"/> | <colorBadge text="13" type="blue"/> |

## Registration

| Feature         |  Manual  |                                 Automated                                 |    Total    |
| --------------- | :------: | :-----------------------------------------------------------------------: | :---------: |
| Launch App      |    -     | <Badge text="Positive" type="tip"/> <Badge text="Negative" type="error"/> |      1      |
| Registration    |    -     |                    <Badge text="Positive" type="tip"/>                    |     0.5     |
| **Percentages** | `0/2=0%` |                                `1.5/2=75%`                                | `1.5/2=75%` |

## Log In & Out

| Feature         |  Manual  |              Automated              |   Total   |
| --------------- | :------: | :---------------------------------: | :-------: |
| Log In          |    -     | <Badge text="Positive" type="tip"/> |    0.5    |
| Log Out         |    -     | <Badge text="Positive" type="tip"/> |    0.5    |
| **Percentages** | `0/2=0%` |              `1/2=50%`              | `1/2=50%` |

## Recipes

| Feature         |  Manual  |              Automated              |    Total    |
| --------------- | :------: | :---------------------------------: | :---------: |
| Browse          |    -     | <Badge text="Positive" type="tip"/> |     0.5     |
| View            |    -     | <Badge text="Positive" type="tip"/> |     0.5     |
| Save            |    -     | <Badge text="Positive" type="tip"/> |     0.5     |
| **Percentages** | `0/3=0%` |             `1.5/3=50%`             | `1.5/3=50%` |

## Notes & Adjustments

| Feature         |  Manual  |                                 Automated                                 |   Total    |
| --------------- | :------: | :-----------------------------------------------------------------------: | :--------: |
| Notes           |    -     | <Badge text="Positive" type="tip"/> <Badge text="Negative" type="error"/> |     1      |
| Adjustment      |    -     | <Badge text="Positive" type="tip"/> <Badge text="Negative" type="error"/> |     1      |
| **Percentages** | `0/2=0%` |                                `2/2=100%`                                 | `2/2=100%` |
