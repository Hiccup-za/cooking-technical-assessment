# Cooking Technical Assessment

[Click here to be taken to the deployed report.](TBA)

## Background

As a cooking enthusiast I want to have access to the latest recipes and add them to my account.
It is important that I am able to adjust the recipes on my account and add notes or adjustments to satisfy my personal taste.

## Tasks

Please create test cases that will cover the above mentioned scenario.
The assignment is open-ended and requires basic functionality and coverage.
If possible use your imagination to add value and identify critical functionality.

## Requirements & Extra Information

- Provide a basic test plan
- Provide test cases
- Provide and an example of a logged issue
- What tools would you use and why
- Identify four testing best practices that you would apply and why
- Automation OR Manual testing? Motivate your answer for the above scenario.
- Provide an extract of example code for ONE one of the test cases.

## Expected Outputs

- Report containing the answers to the requirements above
- How long you took to complete the assignment
- If you had more time, what would you do differently? Also, what would you have added additionally?
- List any assumptions you have before starting with the project
