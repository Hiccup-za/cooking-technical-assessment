# Issue Example

The following is an example of a bug logged found during testing:

## Summary

The [Launch app with no internet connection](https://gitlab.com/Hiccup-za/cooking-technical-assessment/-/blob/master/feature-files/launch-negative.feature) test case has failed on a physical device.

## Prerequisites

- App Version: 0.1.5
- Physical Device: iPhone 11
- OS: 13.3.1

## Steps to Reproduce

1. Install version `0.1.5` of the app on the device mentioned above
2. Activate aeroplane mode
3. Launch the app

## Results

The application will launch and the offline warning does not display.

## Expected Results

The application should launch and display the offline warning.
