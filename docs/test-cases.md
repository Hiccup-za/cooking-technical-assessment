# Test Cases

## Positive Test Cases

### TC-1: Launch App Successfully <colorBadge text="Automated" type="lightGreen"/>

1. The user taps the app icon.  
1. The application launches and pulls the users profile from the database if registered.  
1. If the user is not registered the app will display the `Register` screen.

### TC-2: Register User <colorBadge text="Automated" type="lightGreen"/>

- The user has tapped the app icon.  
- The application launches in an `Unregistered` state and displays the `Register` screen.

1. The user enters their information correctly and taps the `Register` button.  
1. The application completes the register process.  
1. The app then displays the `Recipe Browser` screen.

### TC-3: Unregister User <colorBadge text="Automated" type="lightGreen"/>

- The user has tapped the app icon.  
- The application launches in a `Registered` and `Logged In` state.

1. The user navigates to the `Settings` screen and taps the `Unregister` button.  
1. The application displays an `Are you sure you want to Unregister?` message with an `Unregister` button and `No` button.  
1. The user taps the `Unregister` button.  
1. The application proceeds by logging the user out and completing the unregister process.  
1. The application displays the `Register` screen once complete.

### TC-4: Log Out <colorBadge text="Automated" type="lightGreen"/>

- The user has tapped the app icon.  
- The application launches in a `Registered` and `Logged In` state.

1. The user navigates to the `Settings` screen and taps the `Log Out` button.  
1. The application displays an `Are you sure you want to Log Out?` message with an `Log Out` button and `No` button.  
1. The user taps the `Log Out` button.  
1. The application logs the user out.  
1. The application displays the `Log In` screen.

### TC-5: Log In <colorBadge text="Automated" type="lightGreen"/>

- The user has tapped the app icon.  
- The application launches in a `Logged Out` state and displays the `Log In` screen.

1. The user enters their information correctly and taps the `Log In` button.  
1. The application completes the log in process.  
1. The application then displays the `Recipe Browser` screen.

### TC-6: Browse for Recipes <colorBadge text="Automated" type="lightGreen"/>

- The user has tapped the app icon.  
- The application launches in a `Registered` and `Logged In` state.  
- The application displays the `Recipe Browser` screen.  

1. The user is able to scroll up and down to view all the available recipes and saved recipes.

### TC-7: View Recipe <colorBadge text="Automated" type="lightGreen"/>

- The user has tapped the app icon.  
- The application launches in a `Registered` and `Logged In` state.  
- The application displays the `Recipe Browser` screen.  

1. The user scrolls down to a recipe.  
1. The user taps the recipe to view it.  
1. The user is able to scroll up and down to see all the details.

### TC-8: Save Recipe <colorBadge text="Automated" type="lightGreen"/>

- The user has tapped the app icon.  
- The application launches in a `Registered` and `Logged In` state.  
- The application displays the `Recipe Browser` screen.  
- The user has scrolled down to a recipe.  
- The user has tapped the recipe to view it.  

1. The user taps the `Save Recipe` button
1. The recipe is saved.

### TC-9: Add Notes <colorBadge text="Automated" type="lightGreen"/>

- The user has tapped the app icon.  
- The application launches in a `Registered` and `Logged In` state.  
- The application displays the `Recipe Browser` screen.  
- The user has scrolled down to a saved recipe.  
- The user has tapped the recipe to view it.  

1. The user taps the `Add Note` button.  
1. The `Add Note` pop-up displays.  
1. The user types in their note.  
1. The user taps the `Save` button.  
1. The note is saved.

### TC-10: Make Adjustment <colorBadge text="Automated" type="lightGreen"/>

- The user has tapped the app icon.  
- The application launches in a `Registered` and `Logged In` state.  
- The application displays the `Recipe Browser` screen.  
- The user has scrolled down to a saved recipe.  
- The user has tapped the recipe to view it.  

1. The user taps the `Make Adjustment` button.  
1. The `Make Adjustment` editor displays.  
1. The user makes their adjustments.  
1. The user taps the `Save` button.  
1. The adjustment is saved.  
1. The `Adjusted` tag is displayed on the saved recipe.

## Negative Test Cases

### TC-11: Launch app with no internet connection <colorBadge text="Automated" type="lightGreen"/>

- The mobile device is in a state with no internet connection.

1. The user taps the app icon.  
1. The application launches in an `Offline` state.  
1. The application displays an offline warning.  
1. The applications functionality should become inactive.

### TC-12: Save an Empty Note <colorBadge text="Automated" type="lightGreen"/>

- The user has tapped the app icon.  
- The application launches in a `Registered` and `Logged In` state.  
- The application displays the `Recipe Browser` screen.  
- The user has scrolled down to a saved recipe.  
- The user has tapped the recipe to view it.  

1. The user taps the `Add Note` button.  
1. The `Add Note` pop-up displays.  
1. The user does not type anything in.  
1. The `Save` button should be inactive, guiding the user to tap the `Cancel` button instead.

### TC-13: Save an Recipe after making no Adjustments <colorBadge text="Automated" type="lightGreen"/>

- The user has tapped the app icon.  
- The application launches in a `Registered` and `Logged In` state.  
- The application displays the `Recipe Browser` screen.  
- The user has scrolled down to a saved recipe.  
- The user has tapped the recipe to view it.  

1. The user taps the `Make Adjustment` button.  
1. The `Make Adjustment` editor displays.  
1. The user does not type anything in.  
1. The `Save` button should be inactive, guiding the user to tap the `Cancel` button instead.
