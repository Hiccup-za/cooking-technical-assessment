# Manual OR Automation

In my view the goal would always be to automate and mock components out as much as possible.

Maintaining manual test specifications these days can be a nightmare, especially as functionality slowly grows over time.
If we keep my assumption in mind where the user is using a mobile application; every year brings dozens of new mobile devices to the market.
This means that the total number of devices needed for manual testing quickly grows to the exhaustive testing point.
Its simply not scaleable.

Naturally there will always be some aspects of testing that cannot be easily automated.
Aspects like User Experience come to mind.

But for the most part, my perspective is automate as much as possible.
