# Test Plan

## Requirement

As a cooking enthusiast I want to have access to the latest recipes and add them to my account.
It is important that I am able to adjust the recipes on my account and add notes or adjustments to satisfy my personal taste.

### Assumptions

- The cooking enthusiast is using a mobile application to view and save recipes
- The mobile application displays recipes which are curated and published by non-users
- The mobile application is an iOS application
- The cooking enthusiast has an account which means a database of some sorts is required to store account information as well as the recipes
- A REST API is used

### Requirement Validation

| Requirement                            | Test Case Link                                                   |
| -------------------------------------- | ---------------------------------------------------------------- |
| Access to the latest recipes           | [TC-6][6], [TC-7][7], [TC-11][11]                                |
| Add recipes to my account              | [TC-1][1], [TC-2][2], [TC-3][3], [TC-4][4], [TC-5][5], [TC-8][8] |
| Add notes to a specific recipe         | [TC-9][9], [TC-12][12]                                           |
| Make adjustments to as specific recipe | [TC-10][10], [TC-13][13]                                         |

## Scope

The scope of this Test Plan is for the `Frontend` aspect of the mobile application.

### Features in Scope

The following features are in scope:

- Registration
- Log In & Out
- Recipes
- Notes & Adjustments

### Features Out of Scope

The following features are out of scope:

- Sharing recipes
- Sharing adjusted recipes
- Sharing recipes with notes
- Sharing adjusted recipes with notes
- Offline usage

### Supported Devices

The mobile application is supported on the following iOS devices:

|           Device            |       OS        |
| :-------------------------: | :-------------: |
|          iPhone 11          |    iOS 13.0     |
|   iPhone 11 Pro / Pro Max   |    iOS 13.0     |
|     iPhone 12 / 12 Mini     | iOS 14.1 / 14.2 |
| iPhone 12 Pro / 12 Mini Pro | iOS 14.1 / 14.2 |

### Unsupported Devices

All iOS devices older than the `iPhone 11` and with an OS version older than `iOS 13.0` is unsupported.

::: danger Android Devices
All Android devices are currently unsupported as an Android version of the mobile application does not exist.
:::

## Environment Configuration

The following environments exist for this mobile application:

- Local
- Pre-Production
- Production

### Local

The local environment is on my MacBook where I can spin up emulators of our supported devices using BrowserStack and test the mobile application.

### Pre-Production

Once builds of the application have passed testing in the local environment, they will be published to Test Flight.
This will enable testing to take place on physical devices to give confidence that everything functions as expected before going live.

### Production

Production refers to the live version of the mobile application in the iOS App Store.
Sanity checks can be done if required.

## Testing

### Included Testing Activities

#### Manual Testing

Manual testing will take place when test cases cannot be automated and when absolutely necessary.
This would involve manually executing the test cases on physical devices.

Manual Test Specifications will be documented in Markdown and committed to this repository.
This allows the development team and stakeholders to review and approve all Test Specifications.

#### Automation Testing

The primary goal is to automate as many of the test cases as possible.
Automated tests will be executed on merge to Master and feature branches, ensuring that regression does not take place during development.

#### Exploratory Testing

Exploratory testing will take place in Pre-Production using Test Flight.
This would involve using physical devices within our coverage scope.

### Excluded Testing Activities

The following testing activities are not in scope and will therefore be excluded:

- Penetration Testing
- Performance Testing
- Reliability Testing
- Chaos Testing

## Tools

The following tools will be used:

### BrowserStack

[BrowserStack](https://www.browserstack.com) offers [Automated Mobile App Testing](https://www.browserstack.com/app-automate) and this will be used as the primary automation tool.

### Test Flight

Test Flight will be used for final spot checks and exploratory testing prior to uploading the build to the App Store.

### Markdown

Writing Quality & Testing artifacts in [Markdown](https://www.markdownguide.org/) and committing them into a repository enables the entire team and stakeholders to review everything, just like code review.

### VuePress

[VuePress](https://vuepress.vuejs.org/) is a static site generator which renders markdown files as HTML, allowing Quality & Testing artifacts such as Manual Test Specifications to be readable and actionable in the absence of a test management tool.

[1]: /test-cases.md#tc-1-launch-app-successfully
[2]: /test-cases.md#tc-2-register-user
[3]: /test-cases.md#tc-3-unregister-user
[4]: /test-cases.md#tc-4-log-out
[5]: /test-cases.md#tc-5-log-in
[6]: /test-cases.md#tc-6-browse-for-recipes
[7]: /test-cases.md#tc-7-view-recipe
[8]: /test-cases.md#tc-8-save-recipe
[9]: /test-cases.md#tc-9-add-notes
[10]: /test-cases.md#tc-10-make-adjustment
[11]: /test-cases.md#tc-11-launch-app-with-no-internet-connection
[12]: /test-cases.md#tc-12-save-an-empty-note
[13]: /test-cases.md#tc-13-save-an-recipe-after-making-no-adjustments
